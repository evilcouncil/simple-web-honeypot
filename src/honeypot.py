"""Runs an exceedingly simple honeypot."""

from flask import Flask
from flask import session
from flask import render_template
from flask import request
from flask import url_for
from flask import redirect
from loguru import logger
import argparse
import json

app = Flask(__name__, static_url_path='')

logger.add("honeypot.log", rotation="500 MB")

def _generate_log_dict(request):
    data = dict()
    data['url'] = request.url
    data['ip'] = request.remote_addr
    data['user-agent'] = str(request.user_agent)
    data['method'] = request.method
    headers = list()
    for header in request.headers:
        headers.append(str(header))
    data['headers'] = headers
    return data



@app.route('/uploads', methods=['POST'])
def uploads():
    data = _generate_log_dict(request)
    data['command'] = request.form['command']
    logger.info(json.dumps(data))

    return render_template('index.html')


@app.route('/', defaults={'path': ''})
@app.route("/<string:path>")
@app.route('/<path:path>')
def index(path):
    data = _generate_log_dict(request)
    logger.info(json.dumps(data))
    return render_template("index.html")


if __name__ == "__main__":
    parser = argparse.ArgumentParser("honeypot")
    parser.add_argument("--port", default=8080, type=int, help="what port to listen on")
    parser.add_argument("--session-key", default='Totally_Secret_S3ssion_key', help="Session key to use, for all of the actual data being sent to this app")
    args = parser.parse_args()
    app.secret_key = args.session_key
    app.run(host="0.0.0.0", port=args.port, debug=False)
